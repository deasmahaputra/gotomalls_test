<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index(Request $request){
		if ($request->get('start_date') && $request->get('end_date')){

			// get data from request
			$start_date = $request->get('start_date');
			$end_date = $request->get('end_date');

			// url data
			$URL = "https://www.googleapis.com/calendar/v3/calendars/id.indonesian%23holiday%40group.v.calendar.google.com/events?key=AIzaSyA8PXrbSfVrZpNswjF9221j9i1w0QIqTjk&timeMin=".$start_date."T00:00:00-07:00&timeMax=".$end_date."T23:59:00-07:00";
			
			// perform 'GET' request data to calendar api
			$client = new \GuzzleHttp\Client();
			$res = $client->request('GET', $URL);
			
			// serialize json data
			$data = json_decode($res->getBody(), false);

            return view('home', ['events' => $data->items, 'start_date'=> $start_date, 'end_date' => $end_date]);
        } else {
			return view('home', ['events' => [], 'start_date'=> "", 'end_date' => '']);

		}
	}
}
