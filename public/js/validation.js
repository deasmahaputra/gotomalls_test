
	$(document).ready(function(){
		var container= $('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
		$('#start_date').datepicker({
			format: 'yyyy-mm-dd',
			container: container,
			todayHighlight: true,
			autoclose: true,

		});
		$('#end_date').datepicker({
			format: 'yyyy-mm-dd',
			container: container,
			todayHighlight: true,
			autoclose: true,
		});
	});



	$(function(){
		$("#startdate_error").hide();
		$("#enddate_error").hide();

		var startdate_error = false;
		var enddate_error = false;

		$("#start_date").focusout(function(){
			check_startdate();
		});

		$("#end_date").focusout(function(){
			check_enddate();
		});

		function check_startdate(){
			var startdate_length = $("#start_date").val().length;

			if(startdate_length == 0){
				$("#startdate_error").html("start date cannot be empty");
				$("#startdate_error").show();
				startdate_error = true;
			}else{
				$("#startdate_error").hide();
			}
		}

		function check_enddate(){
			var enddate_length = $("#end_date").val().length;

			if(enddate_length == 0){
				$("#enddate_error").html("end date cannot be empty");
				$("#enddate_error").show();
				enddate_error = true;
			}else{
				$("#enddate_error").hide();
			}
		}

		$("#calendar_indonesia").submit(function(){
			startdate_error = false;
			enddate_error = false;

			check_startdate();
			check_enddate();


			if(startdate_error == false && enddate_error == false){
				return true;
			}else{
				return false;
			}
		});
	});