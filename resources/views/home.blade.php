<!DOCTYPE html>
<html lang="en">
<head>
  	<meta http-equiv="content-type" content="text/html; charset=UTF-8">
  	<meta charset="utf-8">
  	<title>Kalender Liburan Indonesia</title>
  	<link rel="stylesheet" href="https://formden.com/static/cdn/bootstrap-iso.css" />
	<link rel="stylesheet" href="https://formden.com/static/cdn/font-awesome/4.4.0/css/font-awesome.min.css" />
	<link rel="stylesheet" type="text/css" href="{{ URL::asset('css/style.css') }}"/>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>
</head>
<body>

<div class="container-logo">
	<div class="col-md-6 col-sm-6 col-xs-12">
		<img src="{{ URL::asset('img/logo.png') }}" alt="">
		<br>
		<span>Pilih tanggal untuk melihat hari libur di Indonesia</span>
	</div>
</div>
<div class="bootstrap-iso">
	<div class="container-fluid">
  		<div class="row">
   			<div class="col-md-6 col-sm-6 col-xs-12">
    			<form class="form-horizontal" method="get" id="calendar_indonesia">
     				<div class="form-group ">
      					<label class="control-label col-sm-2 requiredField" for="date">Date Start</label>
      					<div class="col-sm-10">
       						<div class="input-group">
					        <div class="input-group-addon">
						         <i class="fa fa-calendar"></i>
					        </div>
				        	<input class="form-control" id="start_date" name="start_date" placeholder="MM/DD/YYYY" type="text" value="{{ $start_date }}"/>
				        	
				       		</div>
				       		<span id="startdate_error" class="date_error"></span>
				      	</div>
     				</div>
					<div class="form-group ">
						<label class="control-label col-sm-2 requiredField" for="date">Date End</label>
						<div class="col-sm-10">
							<div class="input-group">
								<div class="input-group-addon">
									<i class="fa fa-calendar"></i>
								</div>
								<input class="form-control" id="end_date" name="end_date" placeholder="MM/DD/YYYY" type="text" value="{{ $end_date }}"/>
								
							</div>
							<span id="enddate_error" class="date_error"></span>
						</div>
					</div>
					<div class="form-group">
				      	<div class="col-sm-10 col-sm-offset-2">
				       		<button class="btn btn-primary" type="submit">Submit</button>
				      	</div>
				    </div>
    			</form>
   			</div>
  		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="list-group" style="max-width: 500px">
					@if(count($events) == 0)
						<div class="alert alert-warning">No event holiday in this range date</div>
					@else
						@foreach ($events as $event)
							<a href="{{ $event->htmlLink }}" class="list-group-item">
								<h4 class="list-group-item-heading" style="color: #21618C;">{{ $event->summary }}</h4>
								<p class="list-group-item-text">
								<p>Date: {{ $event->start->date }}</p>
								<!-- <p>End Date: {{ $event->end->date }}</p> -->
								<p style="font-size: 12px; font-style: italic;">Selamat melaksanakan {{ $event->summary }}</p>
							</a>
						@endforeach
					@endif
				</div>
			</div>
		</div>
 	</div>
</div>
<div class="footer"></div>
<script type="text/javascript" src="https://formden.com/static/cdn/formden.js"></script>
<script type="text/javascript" src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript" src="{{ URL::asset('js/validation.js') }}"></script>



</body>

</html>